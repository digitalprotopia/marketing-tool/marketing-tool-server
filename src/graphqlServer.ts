import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import express from 'express';
import cors from 'cors';
import http from 'http';
import jwt from 'jsonwebtoken';
import gql from 'graphql-tag';
import ac from './pemission';
import { Permission } from 'role-acl';
import { IClient } from './entities/IClient';
import { BaseResolver } from './resolvers/baseResolvers';
import { userResolver } from './resolvers/UserResolver';
import ClientRepository from './repositories/ClientRepository';
import { authResolver } from './resolvers/AuthResolver';
import config from './config/config';
import { AuthRepository } from './repositories/AuthRepository';
import SessionRepository from './repositories/SessionRepository';
import { ISession } from './entities/ISession';
import { IUser } from './entities/IUser';
import { ApolloServerPluginLandingPageGraphQLPlayground } from '@apollo/server-plugin-landing-page-graphql-playground';

const resolvers:BaseResolver[] = [userResolver, authResolver];

interface LicenseRepositoryContext {
  client?: IClient;
}

interface MyFragmentDefenition {
  operation: string
}

const startApollo = async () => {
  const app = express();
  const httpServer = http.createServer(app);
  const server = new ApolloServer<LicenseRepositoryContext>({
    typeDefs: resolvers.map((resolver) => resolver.getSchema()),
    resolvers: resolvers.map((resolver) => resolver.getResolvers()),
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer }), ApolloServerPluginLandingPageGraphQLPlayground()],
  });

  await server.start();
  
  app.use(
    '/graphql',
    cors(),
    express.json(),
    expressMiddleware(server, {
      context: async ({ req }) => {
        const token = req.headers.authorization?.replace('Bearer ', '');

        let authNeeded = false;
        const definitions:readonly MyFragmentDefenition[] = (gql`
        ${req.body.query}
      `.definitions as readonly MyFragmentDefenition[]);
  
        const operations:string[] = [];
  
        (definitions).forEach((definition: any) => {
          if (definition.operation?.toLowerCase() === 'mutation' || definition.operation?.toLowerCase() === 'query') {
            definition.selectionSet.selections.forEach((selection: any) => {
              operations.push(selection.name.value);
            });
          }
        });
        
        operations.forEach((operation) => {
          const permission:Permission = ac.can('guest').execute(operation).sync().on('*') as Permission;
          if (!permission.granted) {
            authNeeded = true;
          }
        });

        if (!authNeeded) {
          return { client: null, user: null };
        }
        
        if (!token) {
          throw new Error('Invalid token');
        }
        const decoded = jwt.decode(token || '', { json: true });
        if (!decoded) {
          throw new Error('Invalid token');
        }
        if (decoded.type === 'client') {
          const client = (await new ClientRepository().getByFields({ id: decoded?.sub || '' }))[0];
          if (!client) {
            throw new Error('Invalid token');
          }
          try {
            jwt.verify(token || '', client?.secret || '');
          } catch (e) {
            throw new Error('Invalid token');
          }
          operations.forEach((operation: any) => {
            const permission:Permission = ac.can(client.type).execute(operation).sync().on('*') as Permission;
            if (!permission.granted) {
              throw new Error('Permission denied');
            }
          });
          return { client };
        }
        if (decoded.type === 'user') {
          let session: ISession;
          let user: IUser;
          try {
            jwt.verify(token, config.jwt.secret_key);
            try {
              const sessionRepository = new SessionRepository(undefined, decoded.session_id);
              await sessionRepository.check(decoded.sub || '');
              session = await sessionRepository.getData();
            } catch {
              throw new Error('Invalid token');
            }
            user = (await new AuthRepository(undefined, decoded.sub).getData());
          } catch {
            throw new Error('Invalid token');
          }
          user.id = decoded.sub || '';
          operations.forEach((operation: any) => {
            const permission:Permission = ac.can(user.role).execute(operation).sync().on('*') as Permission;
            if (!permission.granted) {
              throw new Error('Permission denied');
            }
          });
          return { user, session };
        }
        throw new Error('Invalid token');
      },
    }));

  httpServer.listen({ port: 4000 });
  console.log('🚀 Server ready at http://localhost:4000/graphql');
};

export default startApollo;