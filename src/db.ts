import knex from 'knex';
import config from './config/config';

const db = knex({
  client: 'pg',
  connection: {
    connectionString: config.db.connection,
  },
  pool: {
    min: 0,
    max: 10,
    acquireTimeoutMillis: 60000,
    idleTimeoutMillis: 600000,
  },
});

export default db;