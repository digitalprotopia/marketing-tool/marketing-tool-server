export interface IEntity {
  id: string,
  createdAt?: Date,
  updatedAt?: Date,
  deletedAt?: Date,
  isDeleted?: boolean
}