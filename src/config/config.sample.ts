export interface Config {
  db: {
    connection: string;
  };
  emailTransporter: {
    host: string;
    port: number;
    auth: {
      user: string;
      pass: string;
    };
  };
  host: string;
  jwt: {
    secret_key: string;
  };
}

const config:Config = {
  db: {
    connection: '',
  },
  emailTransporter: {
    host: '',
    port: 465,
    auth: {
      user: '',
      pass: '',
    },
  },
  host: '',
  jwt: {
    secret_key: '',
  },
};

export default config;