import { Config } from './config.sample';

const config:Config = {
  db: {
    connection: process.env.DB_CONNECTION!,
  },
  emailTransporter: {
    host: process.env.EMAIL_HOST!,
    port: parseInt(process.env.EMAIL_PORT!),
    auth: {
      user: process.env.EMAIL_USER!,
      pass: process.env.EMAIL_PASSWORD!,
    },
  },
  host: process.env.HOST!,
  jwt: {
    secret_key: process.env.JWT_SECRET_KEY!,
  },
};

export default config;