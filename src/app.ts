import jwt from 'jsonwebtoken';
import startApollo from './graphqlServer';
import { IClient } from './entities/IClient';
import ClientRepository from './repositories/ClientRepository';

const serverStart = async function () {
  await startApollo();
  const clients:IClient[] = await new ClientRepository().getAll();
  clients.forEach((client) => {
    const token = jwt.sign({ sub: client.id, type: 'client' }, client.secret);
    console.log(`Client ${client.name} token: ${token}`);
  });
};

serverStart();
