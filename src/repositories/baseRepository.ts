import { Knex } from 'knex';
import db from '../db';
import { IEntity } from '../entities/IEntity';
import { IUser } from '../entities/IUser';

abstract class BaseRepository<T extends IEntity> {

  id?: string;

  _data?: Promise<T>;

  contextUser?: IUser;

  isInternal = true;

  get data(): Promise<T> | undefined {
    return this.getData();
  }

  constructor(contextUser: IUser | undefined = undefined, id: string | T | undefined = undefined, isInternal = false) {
    this.isInternal = isInternal;
    if (!this.isInternal && this.needContextUser() !== undefined) {
      if (!!contextUser !== this.needContextUser()) {
        throw new Error('Access denied');
      }
    }
    this.contextUser = contextUser;
    if (id && typeof id === 'string') {
      this.id = id;
    } else if (id && typeof id === 'object') {
      this.id = id.id;
      this._data = Promise.resolve(id);
    }
  }
  
  abstract getTable(): string;

  needContextUser(): boolean | undefined {
    return undefined;
  }

  async checkEntityAccess(entity: Partial<T>) {
    return !!entity;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async checkEntityData(entity: Partial<T>, isCreate: boolean = false) {
    return !!entity;
  }

  async checkEntities(ids: string[]): Promise<T[]> {
    const entities = await this.getByIds(ids);
    if (entities.length !== ids.length) {
      throw new Error('Access denied');
    }
    return entities;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async checkEntitiesOfOrganization(ids: string[], organizationId: string): Promise<T[]> {
    const entities = await this.getByIds(ids);
    return entities;
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    return knex;
  }
  
  async getData(update: boolean = false): Promise<T> {
    if (!update && this._data && await this._data) return this._data as Promise<T>;
    if (!this.id) throw new Error('Id not found');
    const entity = await db.select('*').from(this.getTable()).where('id', this.id)
      .andWhere('isDeleted', false)
      .first();
    if (!entity) throw new Error('Entity not found');
    await this.checkEntityAccess(entity);
    this._data = entity;
    return entity;
  }

  orderBy(): string {
    return `${this.getTable()}.createdAt`;
  }

  async getAll(): Promise<T[]> {
    const result = await this.filterEntities(db.select(`${this.getTable()}.*`).from(this.getTable())
      .where(`${this.getTable()}.isDeleted`, false)
      .orderBy(this.orderBy(), 'asc'));
    return result;
  }

  async getByIds(ids: string[]): Promise<T[]> {
    const result = await this.filterEntities(db.select(`${this.getTable()}.*`)
      .from(this.getTable()).whereIn(`${this.getTable()}.id`, ids)
      .andWhere(`${this.getTable()}.isDeleted`, false)
      .orderBy(this.orderBy(), 'asc'));
    return result;
  }

  async getByFields(fields: Partial<T>): Promise<T[]> {
    const result = await this.filterEntities(db.select(`${this.getTable()}.*`)
      .from(this.getTable()).where(fields)
      .andWhere(`${this.getTable()}.isDeleted`, false).orderBy(this.orderBy(), 'asc'));
    return result;
  }

  async getByQuery(callback: (knex: Knex.QueryBuilder) => Knex.QueryBuilder): Promise<T[]> {
    const result = await this.filterEntities(
      callback(
        db.select(`${this.getTable()}.*`).from(this.getTable())
          .where(`${this.getTable()}.isDeleted`, false)
          .orderBy(this.orderBy(), 'asc'),
      ),
    );
    return result;
  }

  async create(entity: Partial<T>): Promise<T> {
    entity.createdAt = new Date();
    await this.checkEntityAccess(entity);
    await this.checkEntityData(entity, true);
    const [created] = await db(this.getTable()).insert(entity).returning('*');
    this.id = created.id;
    return created;
  }

  async edit(entity: Partial<T>): Promise<T> {
    entity.updatedAt = new Date();
    const data = await this.data;
    await this.checkEntityAccess(data as T);
    await this.checkEntityAccess(entity);
    await this.checkEntityData(entity);
    const [edited] = await db(this.getTable()).where('id', this.id).
      andWhere('isDeleted', false).
      update(entity).returning('*');
    await this.getData(true);
    return edited;
  }

  async delete(): Promise<boolean> {
    await this.getData();
    await db(this.getTable()).where('id', this.id).
      andWhere('isDeleted', false).
      update({
        isDeleted: true,
        deletedAt: new Date(),
      });
    return true;
  }
}

export default BaseRepository;