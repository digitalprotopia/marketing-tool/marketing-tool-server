import { IClient } from '../entities/IClient';
import BaseRepository from './baseRepository';

class ClientRepository extends BaseRepository<IClient> {

  getTable(): string {
    return 'Client';
  }
}

export default ClientRepository;