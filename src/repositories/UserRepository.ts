import bcrypt from 'bcrypt';
import db from '../db';
import BaseRepository from './baseRepository';
import { IUser } from '../entities/IUser';
export class UserRepository extends BaseRepository<IUser> {
  getTable(): string {
    return 'User';
  }

  public async create(user: Partial<IUser>) {
    if (!user.password) throw new Error('The transferred user does not have a password');
    // user.email = user.email.trim().toLowerCase();
    let model = await this.getByEmail(user.confirmEmail as string);
    if (model) {
      throw new Error(
        'A user with this email or username already exists',
      );
    }

    const entity = await db('User')
      .where('email', user.confirmEmail)
      .first();
    if (entity) throw new Error('A user with this e-mail address is already in the database');
    return super.create(user);
  }

  public async checkUsersExist(userIds: string[]): Promise<boolean> {
    const users = await this.getByIds(userIds);
    return (users || []).length === userIds.length;
  }

  public async getByEmail(email: string): Promise<IUser | null> {
    return (await this.getByFields({ email }))[0];
  }

  public async getByName(name: string): Promise<IUser | null> {
    return (await this.getByFields({ name }))[0];
  }

  public async getEmailsByIdList(participants: string[]): Promise<string[]> {
    const emails = await db('User')
      .where('id', 'in', participants)
      .where('isDeleted', false)
      .orderBy('createdAt', 'asc')
      .pluck('email');
    return emails;
  }

  public async changePassword(
    oldPassword: string,
    newPassword: string,
  ) {
    const user = await this.data;
    if (!user?.password) throw new Error('The user in the database does not have a password');
    const isPasswordCorrect = await bcrypt.compare(
      oldPassword,
      user.password,
    );
    if (!isPasswordCorrect) {
      throw new Error('The old password is incorrect');
    }

    const newPasswordHash = await bcrypt.hash(newPassword, 10);
    this.edit({ password: newPasswordHash });
    return user;
  }
}
