import crypto from 'crypto';
import { UserRepository } from './UserRepository';
import { transporter } from '../email';
import config from '../config/config';
import { IUser } from '../entities/IUser';

export class ConfirmEmailRepository extends UserRepository {

  public async sendEmailConfirmationLink(email: string) {
    let model: IUser | null;
    if (this.contextUser) {
      model = this.contextUser;
    } else {
      model = (await this.getByFields({ confirmEmail: email }))[0];
    }
    if (!model) {
      throw new Error('Почта уже подтверждена');
    }
    const confirmEmailCode = crypto
      .getRandomValues(new Uint32Array(1))[0]
      .toString(16);
    const confirmEmailExpires = Date.now() + 3600000;
    model.confirmEmailCode = confirmEmailCode;
    model.confirmEmailExpires = new Date(confirmEmailExpires);
    model.confirmEmail = email;
    await new ConfirmEmailRepository(this.contextUser, model.id).edit(model);
    const result = await transporter.sendMail({
      from: `S3APP <${config.emailTransporter.auth.user}>`,
      to: email,
      subject: 'Ссылка на подтверждение почты',
      text: `Ссылка на подтверждение почты: ${config.host}/confirm-email/?code=${confirmEmailCode}`,
    });
    if (result) {
      return true;
    }
    return false;
  }
    
  public async ConfirmEmail(code: string) {
    let model: IUser;
    if (this.contextUser) {
      model = this.contextUser;
      if (model.confirmEmailCode !== code) {
        throw new Error('Временный код не найден');
      }
    } else {
      model = (await this.getByFields({ confirmEmailCode: code }))[0];
      if (!model) {
        throw new Error('Временный код не найден');
      }
    }
    if (
      model.confirmEmailExpires &&
                Date.now() - new Date(model.confirmEmailExpires).getTime() > 3600000
    ) {
      throw new Error('Токен истек');
    }
    if (!model.confirmEmail) {
      throw new Error('Почта не найдена');
    }
    const emailModel = await this.getByEmail(model.confirmEmail);
    if (emailModel) {
      throw new Error('Почта уже подтверждена');
    }
    model.email = model.confirmEmail;
    model.confirmEmail = '';
    model.confirmEmailExpires = undefined;
    model.confirmEmailCode = '';
    if (model.id) {
      await new ConfirmEmailRepository(this.contextUser, model.id).edit(model);
    }
    return true;
  }
}