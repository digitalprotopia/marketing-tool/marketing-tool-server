import { GraphQLScalarType } from 'graphql';
import { IUser } from '../entities/IUser';
import { IClient } from '../entities/IClient';
import { ISession } from '../entities/ISession';
import { addRole } from '../pemission';

export type Resolver<TArguments, TResult> = (parent: object, args:TArguments, contextValue: { user: IUser, client: IClient }, info: object) => TResult | Promise<TResult>;

export const resolver = <TArguments, TResult>(func:Resolver<TArguments, TResult>) => func;

interface ResolverOptions<TArguments, TParent = object> {
  parent: TParent, args: TArguments, user: IUser, info: object, client: IClient, session: ISession 
}

export abstract class BaseResolver {
  resolvers: { 
    [type: string]: { [field: string]: (parent: any, args: any, contextValue: { user: IUser, client: IClient, session: ISession }, info: any) => Promise<any> } | GraphQLScalarType | any,
  } = {};

  schema: string = '';

  constructor() {
    this.setResolvers();
  }

  addResolver<TArguments, TResult, TParent = object>(
    type: string,
    field: string,
    func:(options: ResolverOptions<TArguments, TParent>) => Promise<TResult>) {
    if (!this.resolvers[type]) {
      this.resolvers[type] = {};
    }
    this.resolvers[type][field] = (parent: TParent, args: any, contextValue: { user: IUser, client: IClient, session: ISession }, info: object) => {
      const { user, client, session } = contextValue;
      return func({ parent, args, user, client, session, info });
    };
  }

  addQueryResolver<TArguments, TResult>(
    field: string,
    func:(options: ResolverOptions<TArguments>) => Promise<TResult>,
    roles: string[] = [],
  ) {
    this.addResolver<TArguments, TResult>('Query', field, (options) => func(options));
    roles.forEach((role) => addRole(role, [field]));
  }

  addMutationResolver<TArguments, TResult>(
    field: string,
    func:(options: ResolverOptions<TArguments>) => Promise<TResult>,
    roles: string[] = [],
  ) {
    this.addResolver<TArguments, TResult>('Mutation', field, (options) => func(options));
    roles.forEach((role) => addRole(role, [field]));
  }

  addEntityResolver<TEntity extends object, TResult, TArguments = null>(
    entity: string,
    field: (keyof TEntity & string) | (string & {}),
    func:(options: ResolverOptions<TArguments, TEntity>) => Promise<TResult>) {
    this.addResolver<TArguments, TResult, TEntity>(entity, field, (options) => func(options));
  }

  addScalarType(type: GraphQLScalarType) {
    this.resolvers[type.name] = type;
  }

  abstract setResolvers(): void;

  getResolvers() {
    return this.resolvers;
  }

  setSchema(schema: string) {
    this.schema = schema;
  }

  getSchema() {
    return this.schema;
  }
}