import { IEntity } from '../entities/IEntity';
import { IUser } from '../entities/IUser';
import { UserRepository } from '../repositories/UserRepository';
import { BaseResolver } from './baseResolvers';

class UserResolver extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`
    type UserResDto {
        id: ID
        name: String
    }
    
    type ShortUserResDto {
        id: ID
        name: String
    }
    
    type User {
        id: ID
        name: String
    }

    type UserMe {
        id: ID
        name: String
        role: String
    }

    input UserInput {
        name: String
        email: String
        password: String
    }
    
    input UserUpdateInput {
        name: String
        email: String
        password: String
    }
    
    type Query {
      getUser(id: ID!): ShortUserResDto!
      getUsers: [ShortUserResDto]!
      me: UserMe!
    }
  
    type Mutation {
      createUser(user: UserInput!): User!
      editUser(id: ID!, user: UserInput!): User
      editMe(user: UserInput!): User
      deleteUser(id: ID!): Boolean
    }
  `);

    this.addQueryResolver<{}, IUser[]>('getUsers', 
      ({ args, user }) => new UserRepository(user).getAll(), ['admin']);

    this.addQueryResolver<{ id: string }, IUser | null>('getUser', 
      async ({ args, user }) => new UserRepository(user, args.id).getData(), ['admin']);

    this.addQueryResolver<{}, IUser | null>('me', async ({ args, user }) => {
      if (!user) return null;
      return user;
    }, ['admin', 'user']);

    this.addMutationResolver<{ id: string, user: IUser }, IEntity>('editUser', 
      async ({ args, user }) => new UserRepository(user, args.id).edit(args.user), ['admin']);

    this.addMutationResolver<{ user: IUser }, IEntity>('editMe',
      async ({ args, user }) => new UserRepository(user, user.id).edit(args.user), ['admin', 'user']);

    this.addMutationResolver<{ id: string }, boolean>('deleteUser',
      async ({ args, user }) => new UserRepository(user, args.id).delete(), ['admin']);

  }
}

export const userResolver = new UserResolver();
