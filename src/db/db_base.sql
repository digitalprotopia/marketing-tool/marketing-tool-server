CREATE TABLE IF NOT EXISTS public."User"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    name character varying(255) COLLATE pg_catalog."default",
    email character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    "resetPasswordExpires" timestamp without time zone,
    "resetPasswordCode" character varying(255) COLLATE pg_catalog."default",
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "confirmEmailCode" character varying(255) COLLATE pg_catalog."default",
    "confirmEmail" character varying(255) COLLATE pg_catalog."default",
    "confirmEmailExpires" timestamp without time zone,
    role character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT "User_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Client"
(
    name character varying COLLATE pg_catalog."default" NOT NULL,
    type character varying COLLATE pg_catalog."default" NOT NULL,
    secret character varying COLLATE pg_catalog."default" NOT NULL,
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT client_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Session"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "userId" uuid NOT NULL,
    "createdAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "deletedAt" timestamp without time zone,
    "isDeleted" boolean DEFAULT false,
    "expireAt" timestamp without time zone,
    "clientId" uuid,
    CONSTRAINT "Session_pkey" PRIMARY KEY (id),
    CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);