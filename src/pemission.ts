import { AccessControl } from 'role-acl';

const ac = new AccessControl();

export const addRole = (role: string, attributes: string[]) => {
  attributes.forEach((attribute) => {
    ac.grant(role).execute(attribute).on('*');
  });
};

addRole('guest', ['__schema']);

export default ac;